procedure array_value_del (var arr : daint; elm : longword);
Var
  i, s : longword;
Begin
  s := length(arr);
  for i := elm to s-1 do
    arr[i] := arr[i+1];
  setlength(arr, s - 1);
End;
