procedure log_msg(filename, msg : string);
var
  f : text;
begin
  if not fileExists('log/'+filename+'.log') then text_file_new('log/'+filename+'.log');
  assign(f, 'log/'+filename+'.log');
  append(f);
  writeln(f,'['+DateToStr(date)+'_'+TimeToStr(time)+']'+msg);
  close(f);
end;
