procedure setLine (var frame : array of integer; xa, ya, xb, yb, color, ch: integer);
var
  xd, yd, n, xt, yt : integer;
  k : real;
begin
  xd := xb - xa;
  yd := yb - ya;

  if (xd = 0) and (yd = 0)
  
  then begin
    setChXY(frame, xa, ya, color, ch);
  end
  else begin
    if abs(xa - xb) > abs(ya - yb)
    then begin
      k := yd / xd;
      for n:= 0 to xd do begin
        xt := n+xa;
        yt := round(k*n)+ya;
        if (xt >= 0) and (xt < frame[2]) and (yt >= 0) and (yt < frame[3])
        then setChXY (frame, xt, yt, color, ch);
      end
    end
    else begin
      k := xd / yd;
      for n := 0 to yd do begin
        xt := round((k*n)+xa);
        yt := n+ya;
        if (xt >= 0) and (xt < frame[2]) and (yt >= 0) and (yt < frame[3])
        then setChXY (frame, xt, yt, color, ch);
      end
    end;

    if abs(xa - xb) > abs(ya - yb)
    then begin
      k := yd / xd;
      for n := 0 downto xd do begin
        xt := n+xa;
        yt := round(k*n)+ya;
        if (xt >= 0) and (xt < frame[2]) and (yt >= 0) and (yt < frame[3])
        then setChXY (frame, xt, yt, color, ch);
      end
    end
    else begin
      k := xd / yd;
      for n := 0 downto yd do begin
        xt := round((k*n)+xa);
        yt := n+ya;
        if (xt >= 0) and (xt < frame[2]) and (yt >= 0) and (yt < frame[3])
        then setChXY (frame, xt, yt, color, ch);
      end
    end
  end;
end;
