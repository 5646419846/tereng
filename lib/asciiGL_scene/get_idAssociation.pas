function get_idAssociation (id_associations : array of string; association : string) : word;
var
  i : word;
  e : boolean;
begin
  i := 0;
  e := false;
  repeat
    if association = id_associations[i]
    then begin
      get_idAssociation := i;
      e := true;
    end;
    i := i + 1;
  until (i = length(id_associations)) or e;
end;
