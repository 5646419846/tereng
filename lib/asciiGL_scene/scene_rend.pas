procedure sceneZSort(var scene : daint);
var
  tscene : array of integer;
  i, it, k : longword;
  zt : integer;

begin
  i := 0;
  k := 0;


  repeat
    k := 0;
    zt := 32767;
    repeat
      if (scene[k+3] < zt)
      then begin
        zt := scene[k+3];
        it := k;

      //  writeln('k',k);

      end;
      k := k + 6
    until k >= length(scene)-1;
    scene[it+3] := 32767;
 //   writeln('zt: ',zt);
    if zt < 32757
    then begin
      setlength(tscene, i + 5);
      tscene[i] := scene[it];
      tscene[i+1] := scene[it+1];
      tscene[i+2] := scene[it+2];
      tscene[i+3] := scene[it+4];
      tscene[i+4] := scene[it+5];
      i := i + 5;
    end;
  until round(length(scene)/6) = round(length(tscene)/5);
  scene := Copy (tscene,0,length(tscene));

end;

procedure scene_rend(var frame : daint; scene, data : daint; dataOffset : dalw;  xCam, yCam : integer);
var
  scene_near : array of integer;
  i, k, l : longword;
  id, x, y, z, d, m, rgX, rgY: integer;
  w, h: word;
begin
  i := 0;
  k := 0;
  setlength(scene_near, 0);
  repeat
    id := scene[i];
    x := scene[i+1];
    y := scene[i+2];
    z := scene[i+3];
    d := scene[i+4];
    m := scene[i+5];
    i := i + 6;
    log_msg('bugs','ok7!');
  //  writeln(length(dataOffset));
    l := dataOffset[id];
    log_msg('bugs','ok8!');
    rgX := data[l+2];

    rgY := data[l+3];

    x := x - xCam;
    y := y - yCam;
    w := frame[2];
    h := frame[3];
    if ((x >= 0 - rgX) and (x <= w-1 + rgX))
    and ((y >= 0 - rgY) and (y <= h-1 + rgY))
    then begin
      setlength(scene_near, k+6);
      scene_near[k] := id;
      scene_near[k+1] := x;
      scene_near[k+2] := y;
      scene_near[k+3] := z;
      scene_near[k+4] := d;
      scene_near[k+5] := m;
      k := k + 6;
    end;
  until i >= length(scene);

  gotoxy(1,2);
//  writeln(length(scene_near));
  if length(scene_near) > 0
  then begin
    sceneZSort(scene_near);
    {for n := 0 to length(scene_near)-1 do begin
      writeln(n,'- ',scene_near[n]);
    end;//}
    k := 0;
    repeat
      id := scene_near[k];
      x := scene_near[k+1];
      y := scene_near[k+2];
      d := scene_near[k+3];
      m := scene_near[k+4];
      k := k + 5;
      model_rend (frame, data, dataOffset, id, x, y, d, m);
    until k >= length(scene_near);
    //for n := 0 to length(scene_near)-1 do log_msg('scene_near',intToStr(scene_near[n]));
    //readln();
  end;
end;
