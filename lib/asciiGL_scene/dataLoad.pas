procedure dataLoad (
  var data : daint; var data_offset : dalw; 
	var id_association : dastr;
	path : ansistring;
	association : string
);
var
  f : file of integer;
  d : integer;
  id, i, it, fs, l : longword;
begin
  assign(f, path);
  reset(f);
  fs := filesize(f);
  l := length(data);
  i := l;
  id := length(id_association);
  it := i;
  setlength(data, l+fs+2);
  data [i] := id;
  data [i+1] := fs;
  i := i + 2;
  repeat
    read (f,d);
    data[i] := d;
    i := i + 1;
  until i >= l + fs + 2;
  setlength(data_offset, id + 1);
  data_offset[id] := it;
  setlength(id_association, id + 1);
  id_association[id] := association;
end;
